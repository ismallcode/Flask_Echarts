﻿# Host: localhost  (Version 5.5.53)
# Date: 2019-05-10 13:05:54
# Generator: MySQL-Front 6.1  (Build 1.26)


#
# Structure for table "zp_xl"
#

DROP TABLE IF EXISTS `zp_xl`;
CREATE TABLE `zp_xl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `xl_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='最低学历';

#
# Data for table "zp_xl"
#

/*!40000 ALTER TABLE `zp_xl` DISABLE KEYS */;
INSERT INTO `zp_xl` VALUES (1,'大专'),(2,'中专'),(3,'不限'),(4,'本科'),(5,'中技'),(6,'硕士'),(7,'高中');
/*!40000 ALTER TABLE `zp_xl` ENABLE KEYS */;
