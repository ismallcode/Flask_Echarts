﻿# Host: localhost  (Version 5.5.53)
# Date: 2019-05-10 13:06:12
# Generator: MySQL-Front 6.1  (Build 1.26)


#
# Structure for table "zp_zwlb_big"
#

DROP TABLE IF EXISTS `zp_zwlb_big`;
CREATE TABLE `zp_zwlb_big` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zwlb_big_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='职位大类别';

#
# Data for table "zp_zwlb_big"
#

/*!40000 ALTER TABLE `zp_zwlb_big` DISABLE KEYS */;
INSERT INTO `zp_zwlb_big` VALUES (1,'销售业务'),(2,'商超/酒店/娱乐管理/服务');
/*!40000 ALTER TABLE `zp_zwlb_big` ENABLE KEYS */;
