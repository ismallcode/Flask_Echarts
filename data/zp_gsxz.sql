﻿# Host: localhost  (Version 5.5.53)
# Date: 2019-05-10 13:05:32
# Generator: MySQL-Front 6.1  (Build 1.26)


#
# Structure for table "zp_gsxz"
#

DROP TABLE IF EXISTS `zp_gsxz`;
CREATE TABLE `zp_gsxz` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gsxz_name` varchar(100) DEFAULT NULL COMMENT '公司性质',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COMMENT='公司性质';

#
# Data for table "zp_gsxz"
#

/*!40000 ALTER TABLE `zp_gsxz` DISABLE KEYS */;
INSERT INTO `zp_gsxz` VALUES (1,'上市公司'),(2,'民营'),(3,'股份制企业'),(4,'合资'),(5,'其它'),(6,'港澳台公司'),(7,'国企'),(8,'外商独资'),(9,'事业单位'),(10,'保密'),(11,'代表处'),(12,'学校/下级学院'),(13,'国家机关'),(14,'社会团体'),(15,'医院'),(16,'律师事务所'),(17,'银行');
/*!40000 ALTER TABLE `zp_gsxz` ENABLE KEYS */;
