﻿# Host: localhost  (Version 5.5.53)
# Date: 2019-05-10 13:06:01
# Generator: MySQL-Front 6.1  (Build 1.26)


#
# Structure for table "zp_zwlb"
#

DROP TABLE IF EXISTS `zp_zwlb`;
CREATE TABLE `zp_zwlb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zwlb_name` varchar(100) DEFAULT NULL,
  `zwlb_big_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `zwlb_big_id` (`zwlb_big_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='职位类别';

#
# Data for table "zp_zwlb"
#

