﻿# Host: localhost  (Version 5.5.53)
# Date: 2019-05-10 13:04:47
# Generator: MySQL-Front 6.1  (Build 1.26)


#
# Structure for table "zp_gsgm"
#

DROP TABLE IF EXISTS `zp_gsgm`;
CREATE TABLE `zp_gsgm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gsgm_name` varchar(100) DEFAULT NULL COMMENT '公司规模名称',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='公司规模';

#
# Data for table "zp_gsgm"
#

/*!40000 ALTER TABLE `zp_gsgm` DISABLE KEYS */;
INSERT INTO `zp_gsgm` VALUES (1,'20-99人'),(2,'1000-9999人'),(3,'100-499人'),(4,'10000人以上'),(5,'500-999人'),(6,'20人以下'),(7,'保密'),(8,'其他');
/*!40000 ALTER TABLE `zp_gsgm` ENABLE KEYS */;
