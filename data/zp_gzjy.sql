﻿# Host: localhost  (Version 5.5.53)
# Date: 2019-05-10 13:05:45
# Generator: MySQL-Front 6.1  (Build 1.26)


#
# Structure for table "zp_gzjy"
#

DROP TABLE IF EXISTS `zp_gzjy`;
CREATE TABLE `zp_gzjy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gzjy_name` varchar(100) DEFAULT NULL COMMENT '工作经验名称',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COMMENT='工作经验';

#
# Data for table "zp_gzjy"
#

/*!40000 ALTER TABLE `zp_gzjy` DISABLE KEYS */;
INSERT INTO `zp_gzjy` VALUES (1,'不限'),(2,'3-5年'),(3,'1-3年'),(4,'5-10年'),(5,'10年以上'),(6,'1年以下'),(7,'无经验'),(8,'3年以上'),(9,'5年以上'),(10,''),(11,'2年以上'),(12,'1年以上'),(13,'4-8年'),(14,'8年以上'),(15,'4年以上'),(16,'7年以上'),(17,'2-5年');
/*!40000 ALTER TABLE `zp_gzjy` ENABLE KEYS */;
