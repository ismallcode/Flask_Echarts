# coding:utf-8
import pymysql
from flask import Flask  # 默认
from flask_cache import Cache

# from pymysqlpool import ConnectionPool
# app入口定义
app = Flask(__name__)
#链接数据库
mysql_config = {
    'pool_name': 'test',
    'host': 'localhost',
    'port': 3306,
    'user': 'root',
    'password': 'root',
    'database': 'test',
    'charset': 'utf8'
}
# def connection_pool():
#   pool = ConnectionPool(**mysql_config)
#   pool.connect()
#   return pool
conn = pymysql.connect(host='127.0.0.1', user='root', passwd='FanTan879', db='zp', charset='utf8')
cache = Cache()
config = {
  'CACHE_TYPE': 'redis',
    'CACHE_REDIS_HOST': '127.0.0.1',
  'CACHE_REDIS_PORT': 6379,
  'CACHE_REDIS_DB': '1',
  'CACHE_REDIS_PASSWORD': 'FanTan879425'
}
app.config.from_object(config)
cache.init_app(app,config)
app.config.update(SECRET_KEY='tanxiangyu')
